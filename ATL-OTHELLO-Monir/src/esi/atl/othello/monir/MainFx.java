package esi.atl.othello.monir;

import esi.atl.othello.monir.controller.ControllerFx;

/**
 * This class represents the main of the FX version.
 *
 * @author Monir Nasser
 */
public class MainFx {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ControllerFx.othelloLaunch(args);
    }
}
