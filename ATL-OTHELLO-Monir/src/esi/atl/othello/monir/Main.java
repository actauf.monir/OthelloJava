package esi.atl.othello.monir;

import esi.atl.othello.monir.controller.Controller;
import esi.atl.othello.monir.model.Game;
import esi.atl.othello.monir.view.View;

/**
 *
 * @author Monir Nasser
 */
public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        View view = new View();
        Controller controller = new Controller(game, view);
        controller.start();
    }
}
