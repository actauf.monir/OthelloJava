package esi.atl.othello.monir.view;

import esi.atl.othello.monir.model.Board;
import esi.atl.othello.monir.model.Player;
import esi.atl.othello.monir.model.PlayerColor;
import esi.atl.othello.monir.model.Position;
import java.util.Scanner;

/**
 *
 * @author actau
 */
public class View {

    private final Scanner in;

    /**
     * this method initialize the attribut Scanner in
     */
    public View() {
        this.in = new Scanner(System.in);
    }

    /**
     * this method print a message in the begining
     */
    public void initialize() {
        System.out.println("Bienvenue au Jeux OTHELLO");
    }

    /**
     * this method print a player quit the game
     */
    public void quit() {
        System.out.println("Au revoir et Bonne Journée");
    }

    /**
     * this method print the message when we have an error
     *
     * @param message is a String
     */
    public void displayError(String message) {
        System.out.println(message);
    }

    /**
     * this method print the different option for the player
     */
    public void displayHelp() {
        System.out.println("Usage:");
        System.out.println("        type Quit to quit the game.");
        System.out.println("        type Help or ? to see the Menu");
        System.out.println("        type show to show the board");
        System.out.println("        type score to display the number of pieces black and white. ");
        System.out.println("        type play to add a piece in the board.");
        System.out.println();
    }

    /**
     * this method ask one command of the user
     *
     * @return String the string write by the player.
     */
    public String askCommand() {
        System.out.print("\n");
        System.out.println("Veuillez entrer une commande");
        return in.nextLine();
    }

    /**
     * this method print the board
     *
     * @param board
     */
    public void displayBoard(Board board) {
        String[] nameCol = {"a", "b", "c", "d", "e", "f", "g", "h"};
        System.out.print("");
        for (int i = 0; i < board.getNB_COLUMN(); i++) {
            System.out.print("  | " + nameCol[i]);
        }
        System.out.print("  |");
        System.out.println();
        for (int k = 0; k < board.getNB_COLUMN(); k++) {;
            System.out.print("======");
        }
        System.out.println();
        for (int i = 0; i < board.getNB_ROW(); i++) {
            System.out.print(i + " |");
            for (int j = 0; j < board.getNB_COLUMN(); j++) {
                if (board.getPiece(new Position(i, j)) == null) {
                    System.out.print("    |");
                } else {
                    if (board.getPiece(new Position(i, j)).getColor() == PlayerColor.BLACK) {
                        System.out.print(" BL |");
                    } else {
                        System.out.print(" WH |");
                    }
                }
            }
            System.out.println();
        }
    }

    public void displayValuesOfPlayers(int values, Player player) {
        System.out.println("The Value of " + player.getColor() + " is " + values + "\n");
    }

    public void displayCurrentPlayer(Player current) {
        System.out.println();
        if (current.getColor() == PlayerColor.BLACK) {
            System.out.println("The current player is Black");
        } else {
            System.out.println("The current player is White");
        }
    }
}
