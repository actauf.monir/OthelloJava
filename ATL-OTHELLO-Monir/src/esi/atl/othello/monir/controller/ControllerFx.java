package esi.atl.othello.monir.controller;

import esi.atl.othello.monir.model.Action;
import esi.atl.othello.monir.model.Game;
import esi.atl.othello.monir.model.PlayerColor;
import esi.atl.othello.monir.viewfx.DataSide;
import esi.atl.othello.monir.viewfx.InformationWindow;
import esi.atl.othello.monir.viewfx.PlaySide;
import esi.atl.othello.monir.viewfx.StartPage;
import esi.atl.othello.util.Observer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * This class represents the controller of the fx game.
 *
 * @author Monir Nasser.
 */
public class ControllerFx extends Application implements Observer {

    private Game game;
    private PlaySide playside;
    private DataSide dataside;
    private StartPage page1;
    private InformationWindow info;
    private final int width = 950;
    private final int heigth = 650;
    private EventHandler eventWinn;
    private int id = 1;
    private MenuBar menuBar;
    private MenuItem exit;
    private MenuItem information;

    @Override
    public void start(Stage primaryStage) {
        game = new Game();
        game.initialize();
        playside = new PlaySide(game);
        dataside = new DataSide(game);
        page1 = new StartPage();
        eventOnePlayer();
        eventTwoPlayer();
        Scene scene = new Scene(page1, width, heigth);

        game.addObserver(this);

        primaryStage.setTitle("Othello Game");
        primaryStage.setScene(scene);
        eventButtonPlay(primaryStage);
        primaryStage.show();
    }

    /**
     * This method display a new scene for the game.
     *
     * @param primaryStage
     */
    void eventButtonPlay(Stage primaryStage) {
        page1.getPlay().setOnAction((event) -> {
            initMenu();
            VBox root = new VBox(5);
            HBox gamePage = new HBox();
            dataside.getNameP1().setText(page1.getPlayer1().getText());
            dataside.getNameP2().setText(page1.getPlayer2().getText());
            gamePage.getChildren().addAll(playside, dataside);
            BackgroundFill backFill = new BackgroundFill(Color.ANTIQUEWHITE, CornerRadii.EMPTY, Insets.EMPTY);
            Background background = new Background(backFill);
            gamePage.setBackground(background);
            root.getChildren().addAll(menuBar, gamePage);
            Scene scene2 = new Scene(root, width, heigth);
            primaryStage.setScene(scene2);
            eventRestart();
            eventGiveUp(primaryStage);
            InformationAction();
            exitAction();
        });
    }

    /**
     * This method initialize the menu.
     */
    public void initMenu() {
        menuBar = new MenuBar();
        Menu file = new Menu("File");
        exit = new MenuItem("Exit");
        information = new MenuItem("Information Game");
        file.getItems().addAll(information, exit);
        menuBar.getMenus().add(file);
    }

    /**
     * @param args the command line arguments
     */
    public static void othelloLaunch(String[] args) {
        launch(args);
    }

    /**
     * This method update the game view.
     */
    @Override
    public void update() {
        playside.getOthello().updateBoard();
        Action action = new Action(id, dataside.getNameP1().getText(),
                "Place a Piece", playside.getOthello().getX() + "-"
                + playside.getOthello().getY(), 1);
        dataside.updateTableView(action, game, dataside);
        dataside.updateScore(game);
        playside.updateProgressBarAndIndic(game);
        if (info != null) {
            info.updateWindow(game);
        }
        id++;
        eventWinEndGame();
    }

    /**
     * This method doaes the event giv up.
     *
     * @param primaryStage
     */
    void eventGiveUp(Stage primaryStage) {
        playside.getGiveUp().setOnAction((ActionEvent event) -> {
            HBox titre = new HBox(5);
            titre.setAlignment(Pos.CENTER);
            VBox root = new VBox();
            Text texte1 = new Text("CONGRATULATION");
            Text name = new Text();
            Text msg = new Text("You Have Win");
            if (game.getCurrentPlayer().getColor() == PlayerColor.BLACK) {
                name = dataside.getNameP2();
            } else {
                name = dataside.getNameP1();
            }
            titre.getChildren().addAll(texte1, name);
            root.setAlignment(Pos.CENTER);
            root.getChildren().addAll(titre, msg);
            Scene scene3 = new Scene(root, width, heigth);
            primaryStage.setScene(scene3);
        });
    }

    /**
     * This method restart the game.
     */
    void eventRestart() {
        playside.getRestart().setOnAction((event) -> {
            game.resetGame();
            playside.resetDataPlay();
            dataside.resetTableView();
            info.resetWindow();
            id = 1;
        });
    }

    void eventWinEndGame() {
        if (game.isOver()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("CONGRATULATION");
            String texte1 = "CONGRATULATION";
            String name = "";
            String msg = "You Have Win";
            if (game.getWinner().getColor() == PlayerColor.BLACK) {
                name = dataside.getNameP2().getText();
            } else {
                name = dataside.getNameP1().getText();
            }
            alert.setContentText(texte1 + " " + name + "\n" + msg);
            alert.showAndWait();
        }
    }

    void eventOnePlayer() {
        page1.getOnePlayer().setOnAction((event) -> {
            page1.getPlayer2().setDisable(true);
            game.getOpponentPlayer().setComputer(true);
        });
    }

    void eventTwoPlayer() {
        page1.getTwoPlayer().setOnAction((event) -> {
            page1.getPlayer2().setDisable(false);
            game.getOpponentPlayer().setComputer(false);
        });
    }

    /**
     * This method display the new window with the information of the game when
     * he clicked..
     */
    public void InformationAction() {
        information.setOnAction((ActionEvent event) -> {
            info = new InformationWindow(game, dataside);
        });
    }

    /**
     * This method does the exit action on the menu.
     */
    public void exitAction() {
        exit.setOnAction((ActionEvent event) -> {
            System.exit(0);
        });
    }
}
