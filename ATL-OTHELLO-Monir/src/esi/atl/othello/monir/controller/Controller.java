package esi.atl.othello.monir.controller;

import esi.atl.othello.monir.model.Game;
import esi.atl.othello.monir.model.Position;
import esi.atl.othello.monir.view.View;

/**
 * This class represents the controller.
 *
 * @author Monir Nasser
 */
public class Controller {

    private Game game;
    private View view;

    public Controller(Game game, View view) {
        if (game == null || view == null) {
            throw new NullPointerException("The game is null" + game
                    + " or the view " + view);
        }
        this.game = game;
        this.view = view;
    }

    /**
     * This method allows us to start the drawing and keep them still the player
     * hasn't quit.this methods ask what the player wants to do.
     */
    public void start() {
        boolean quit = false;
        game.initialize();
        view.displayBoard(game.getBoard());
        view.displayCurrentPlayer(game.getCurrentPlayer());
        do {
            try {
                String[] command = view.askCommand().toLowerCase().split(" ");
                quit = userChoice(command);
                view.displayCurrentPlayer(game.getCurrentPlayer());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (!quit && !game.isOver());
        view.quit();
    }

    /**
     * This method verify what the player has enter in the command.
     *
     * @param command The string tab.
     * @return boolean if he quit or not.
     */
    public boolean userChoice(String[] command) {
        boolean quit = false;
        switch (command[0]) {
            case "quit":
                quit = true;
                break;
            case "?":
            case "help":
                view.displayHelp();
                break;
            case "show":
                view.displayBoard(game.getBoard());
                break;
            case "score":
                view.displayValuesOfPlayers(game.getScore(game.getCurrentPlayer().getColor()), game.getCurrentPlayer());
                view.displayValuesOfPlayers(game.getScore(game.getOpponentPlayer().getColor()), game.getOpponentPlayer());
                break;
            case "play":
                Position pos = new Position(Integer.parseInt(command[1]), nbLetters(command[2]));
                if (game.isValidMoveAllDirection(pos)) {
                    game.play(pos);
                    view.displayBoard(game.getBoard());
                } else {
                    System.out.println("Invalid Move");
                }
                break;
        }
        return quit;
    }

    /**
     * This method change a string at the corresponding number;
     *
     * @param command The string
     * @return An int.
     */
    public int nbLetters(String command) {
        int k = 0;
        switch (command) {
            case "a":
                k = 0;
                break;
            case "b":
                k = 1;
                break;
            case "c":
                k = 2;
                break;
            case "d":
                k = 3;
                break;
            case "e":
                k = 4;
                break;
            case "f":
                k = 5;
                break;
            case "g":
                k = 6;
                break;
            case "h":
                k = 7;
                break;
            default:
                throw new IllegalArgumentException("The second position"
                        + " is not an integer "
                        + "or the letters is not in the column");
        }
        return k;
    }
}
