package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Action;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 *
 * @author Monir Nasser
 */
public class ActionTable extends TableView {

    public ActionTable() {
        iniTab();
    }

    public void iniTab() {
        TableColumn<Action, Integer> idCol = new TableColumn<Action, Integer>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("Id"));
        idCol.setPrefWidth(40);

        TableColumn<Action, String> nameCol = new TableColumn<Action, String>("NAME");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("Name"));
        nameCol.setPrefWidth(60);

        TableColumn<Action, String> actionCol = new TableColumn<Action, String>("ACTION");
        actionCol.setCellValueFactory(new PropertyValueFactory<>("Action"));
        actionCol.setPrefWidth(130);

        TableColumn<Action, String> positionCol = new TableColumn<Action, String>("POSITION");
        positionCol.setCellValueFactory(new PropertyValueFactory<>("Position"));
        positionCol.setPrefWidth(80);

        TableColumn<Action, Integer> priseCol = new TableColumn<Action, Integer>("PRISE");
        priseCol.setCellValueFactory(new PropertyValueFactory<>("prise"));
        idCol.setPrefWidth(40);
        this.getColumns().setAll(idCol, nameCol, actionCol, positionCol, priseCol);
    }
}
