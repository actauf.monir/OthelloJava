package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Model;
import esi.atl.othello.monir.model.PlayerColor;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * This class represents the view of the left side of the window and also
 * represents the play side.
 *
 * @author Monir Nasser
 */
public final class PlaySide extends VBox {

    private final Othello othello;
    private HBox hButton;
    private HBox hProgBar;
    private HBox hProgInd;
    private Button restart;
    private Button giveUp;
    private ProgressBar pointBar;
    private ProgressIndicator boardIndic;

    public PlaySide(Model game) {
        displayButton(game);
        othello = new Othello(game);
        initProgressBar();
        initProgressIndicator();
        this.getChildren().addAll(othello, hProgBar, hProgInd, hButton);
    }

    /**
     * This method display the button givUp,skip and restart.
     *
     * @param game The model game.
     */
    public void displayButton(Model game) {
        hButton = new HBox(10);
        Button passe = new Button("Skip");
        passe.setOnAction((ActionEvent event) -> {
            game.swapPlayers();
        });
        giveUp = new Button("Give Up");
        restart = new Button("Restart");
        passe.setCenterShape(true);
        giveUp.setCenterShape(true);
        restart.setCenterShape(true);
        hButton.setAlignment(Pos.CENTER);
        hButton.getChildren().addAll(giveUp, passe, restart);
    }

    /**
     * This method display the progress indicator.
     */
    void initProgressIndicator() {
        hProgInd = new HBox(20);
        Text texte = new Text("Completed");
        boardIndic = new ProgressIndicator(0.0625);
        boardIndic.setStyle("-fx-accent: darkgreen;");
        hProgInd.getChildren().addAll(texte, boardIndic);
    }

    /**
     * This method display the progress bar.
     */
    void initProgressBar() {
        hProgBar = new HBox(20);
        Text text = new Text("Black/White");
        pointBar = new ProgressBar(0.5);
        pointBar.setPrefWidth(350);
        pointBar.setStyle("-fx-accent: black;");
        hProgBar.getChildren().addAll(text, pointBar);
    }

    /**
     * This method update the progress bar and the progress indicator.
     *
     * @param game The game model.
     */
    public void updateProgressBarAndIndic(Model game) {
        double nbBlackPiece = game.numberOfPieceOfPlayer(PlayerColor.BLACK);
        double nbWhitePiece = game.numberOfPieceOfPlayer(PlayerColor.WHITE);
        double somme = nbBlackPiece + nbWhitePiece;
        double division = nbBlackPiece / somme;
        pointBar.setProgress(division);
        boardIndic.setProgress(somme / 64);
    }

    /**
     * This method reset the data of the playside.
     */
    public void resetDataPlay() {
        pointBar.setProgress(0.5);
        boardIndic.setProgress(0.0625);
    }

    /**
     * This mehtod allows us to get the othello.
     *
     * @return The board othello.
     */
    public Othello getOthello() {
        return othello;
    }

    /**
     * This method allows us to get the button restart.
     *
     * @return A button.
     */
    public Button getRestart() {
        return restart;
    }

    /**
     * This method allows us to get the button givup.
     *
     * @return the button.
     */
    public Button getGiveUp() {
        return giveUp;
    }
}
