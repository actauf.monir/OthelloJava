package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Model;
import esi.atl.othello.monir.model.PlayerColor;
import esi.atl.othello.monir.model.Position;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 * this class represents the Square
 *
 * @author Monir Nasser
 */
public final class Square extends StackPane {

    private EventHandler<MouseEvent> eventEntered;
    private EventHandler<MouseEvent> eventExited;

    /**
     * This method initialize the square.
     *
     * @param game The game model.
     * @param position The position of the square.
     */
    public Square(Model game, Position position) {
        Rectangle rec = new Rectangle(position.getRow(), position.getColumn(), 60, 60);
        rec.setFill(Color.DARKGREEN);
        rec.setStroke(Color.GRAY);
        Circle piece = createPiece(PlayerColor.BLACK);
        this.getChildren().add(rec);

        if (!game.getBoard().isFree(position)) {
            piece = createPiece(game.getBoard().getPiece(position).getColor());
            this.getChildren().add(piece);
        }
        mouseEventEnteredAndExited(game, rec, piece, position.getRow(), position.getColumn());
    }

    /**
     * This method create a piece.
     *
     * @param color The color of the piece.
     * @return The Piece created.
     */
    Circle createPiece(PlayerColor color) {
        Circle piece = new Circle(27);
        piece.setTranslateX(0);
        if (color == PlayerColor.WHITE) {
            piece.setFill(Color.WHITE);
        } else {
            piece.setFill(Color.BLACK);
        }
        return piece;
    }

    /**
     * This method create the event of the square.
     *
     * @param rec The rectangle.
     * @param game The model game.
     * @param piece The piece.
     */
    void mouseEventEnteredAndExited(Model game, Rectangle rec, Circle piece, int i, int j) {
        Position pos = new Position(i, j);
        eventEntered = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (game.getBoard().isFree(pos)) {
                    if (game.isValidMoveAllDirection(pos)) {
                        rec.setFill(Color.GREEN);
                        if (game.getCurrentPlayer().getColor() != PlayerColor.BLACK) {
                            piece.setFill(Color.WHITE);
                        } else {
                            piece.setFill(Color.BLACK);
                        }
                    } else {
                        rec.setFill(Color.RED);
                        if (game.getCurrentPlayer().getColor() != PlayerColor.BLACK) {
                            piece.setFill(Color.WHITE);
                        } else {
                            piece.setFill(Color.BLACK);
                        }

                    }

                    Square.this.getChildren().add(piece);
                }
            }
        };

        addEventHandler(MouseEvent.MOUSE_ENTERED, eventEntered);

        eventExited = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (game.getBoard().isFree(pos)) {
                    Square.this.getChildren().remove(piece);
                }
                Position pos = new Position((int) rec.getX(), (int) rec.getY());
                if (game.getBoard().getSquare(pos).isBonus()) {
                    rec.setFill(Color.CADETBLUE);
                } else {
                    rec.setFill(Color.DARKGREEN);
                }
            }
        };
        addEventHandler(MouseEvent.MOUSE_EXITED, eventExited);
    }
}
