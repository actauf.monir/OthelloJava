package esi.atl.othello.monir.viewfx;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * This class represents the startPage of the game.
 *
 * @author Monir Nasser
 */
public final class StartPage extends GridPane {

    private Button onePlayer;
    private Button twoPlayer;
    private Button play;
    private Button exit;
    private TextField player1;
    private TextField player2;

    /**
     * This method initialize the page.
     */
    public StartPage() {
        this.setAlignment(Pos.BOTTOM_CENTER);
        this.setPadding(new Insets(20));
        this.setHgap(5);
        this.setVgap(5);
        BackgroundImage backImage = new BackgroundImage(new Image(
                "https://www.lite.games/wp-content/uploads/2017/07/oth_website_feature_1020x435.png",
                950, 650, false, true),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        Background back = new Background(backImage);
        this.setBackground(back);
        initText();
        initTextField();
        initButtonOnePlayer();
        initButtontwoPlayer();
        initButtonPlay();
        initButtonExit();
    }

    /**
     * This method initialize the text.
     */
    void initText() {
        Text namePlayer1 = new Text("PLAYER 1");
        namePlayer1.setUnderline(true);
        namePlayer1.setFont(Font.font(40));
        this.add(namePlayer1, 0, 0);

        Text namePlayer2 = new Text("PLAYER 2");
        namePlayer2.setUnderline(true);
        namePlayer2.setFont(Font.font(40));
        this.add(namePlayer2, 1, 0);
    }

    /**
     * This method initialize the textField.
     */
    void initTextField() {
        player1 = new TextField();
        player1.setPromptText("Name");
        this.add(player1, 0, 1);

        player2 = new TextField();
        player2.setPromptText("Name");
        this.add(player2, 1, 1);
    }

    /**
     * This method initialize the button play.
     */
    void initButtonPlay() {
        play = new Button("Play");
        play.setAlignment(Pos.CENTER);
        play.setCenterShape(true);
        play.setMinWidth(350);
        this.add(play, 0, 5, 2, 1);
    }

    /**
     * This method initialize the button exit..
     */
    void initButtonExit() {
        exit = new Button("Exit");
        exit.setAlignment(Pos.CENTER);
        exit.setCenterShape(true);
        exit.setMinWidth(350);
        exit.setOnAction((ActionEvent event) -> {
            System.exit(0);
        });
        this.add(exit, 0, 6, 2, 1);
    }

    /**
     * This method initialize the button one player;
     */
    void initButtonOnePlayer() {
        onePlayer = new Button("1 Player");
        onePlayer.setAlignment(Pos.CENTER);
        onePlayer.setCenterShape(true);
        onePlayer.setMinWidth(350);
        this.add(onePlayer, 0, 3, 2, 1);
    }

    /**
     * This method initialize the button one player;
     */
    void initButtontwoPlayer() {
        twoPlayer = new Button("2 Player");
        twoPlayer.setAlignment(Pos.CENTER);
        twoPlayer.setCenterShape(true);
        twoPlayer.setMinWidth(350);
        this.add(twoPlayer, 0, 4, 2, 1);
    }

    /**
     * This method returns the button play.
     *
     * @return the button.
     */
    public Button getPlay() {
        return play;
    }

    /**
     * This method returns the textfield of the player1.
     *
     * @return the btextfield.
     */
    public TextField getPlayer1() {
        return player1;
    }

    /**
     * This method returns the textfield of the player2.
     *
     * @return the btextfield.
     */
    public TextField getPlayer2() {
        return player2;
    }

    public Button getOnePlayer() {
        return onePlayer;
    }

    public Button getTwoPlayer() {
        return twoPlayer;
    }
    
    
}
