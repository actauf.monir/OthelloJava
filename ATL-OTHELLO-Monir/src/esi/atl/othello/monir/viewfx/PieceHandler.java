package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Model;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

/**
 *
 * @author Monir Nasser
 */
public class PieceHandler implements EventHandler<MouseEvent>{
    private final Model game;

    public PieceHandler(Model game) {
        this.game = game;
    }
    
    @Override
    public void handle(MouseEvent event) {
        Circle piece = ((Circle) event.getSource());
    }
    
}
