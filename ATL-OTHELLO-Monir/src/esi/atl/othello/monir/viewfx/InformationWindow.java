package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Model;
import esi.atl.othello.monir.model.PlayerColor;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * This represent the informatiuon window.
 *
 * @author Monir Nasser
 */
public final class InformationWindow extends GridPane {

    private Text scoreP1;
    private Text scoreP2;
    private Text nbPiecesP1;
    private Text nbPiecesP2;

    public InformationWindow(Model game, DataSide dataside) {
        this.setPadding(new Insets(10));
        this.setHgap(5);
        this.setVgap(10);
        this.setAlignment(Pos.CENTER);
        initWindow(game, dataside);
    }

    void initWindow(Model game, DataSide dataside) {
        Text namePlayer1 = new Text(dataside.getNameP1().getText());
        Text namePlayer2 = new Text(dataside.getNameP2().getText());
        Label player = new Label("Player ");
        Label score = new Label("Score ");
        Label nbPiece = new Label("Pieces");
        player.setFont(new Font(20));
        score.setFont(new Font(20));
        nbPiece.setFont(new Font(20));
        String scorP1 = Integer.toString(game.getScore(PlayerColor.BLACK));
        String scorP2 = Integer.toString(game.getScore(PlayerColor.WHITE));
        scoreP1 = new Text(scorP1);
        scoreP2 = new Text(scorP2);
        String nbPiecesPB = Integer.toString(game.numberOfPieceOfPlayer(PlayerColor.BLACK));
        String nbPiecesPW = Integer.toString(game.numberOfPieceOfPlayer(PlayerColor.WHITE));
        nbPiecesP1 = new Text(nbPiecesPB);
        nbPiecesP2 = new Text(nbPiecesPW);
        this.add(player, 0, 0);
        this.add(score, 0, 1);
        this.add(nbPiece, 0, 2);
        this.add(namePlayer1, 1, 0);
        this.add(namePlayer2, 2, 0);
        this.add(scoreP1, 1, 1);
        this.add(scoreP2, 2, 1);
        this.add(nbPiecesP1, 1, 2);
        this.add(nbPiecesP2, 2, 2);
        GridPane.setHalignment(scoreP1, HPos.CENTER);
        GridPane.setHalignment(scoreP2, HPos.CENTER);
        GridPane.setHalignment(nbPiecesP1, HPos.CENTER);
        GridPane.setHalignment(nbPiecesP2, HPos.CENTER);
        Scene scene = new Scene(this, 300, 300);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    public void updateWindow(Model game) {
        System.out.println("ok");
        String scorP1 = Integer.toString(game.getScore(PlayerColor.BLACK));
        String scorP2 = Integer.toString(game.getScore(PlayerColor.WHITE));
        scoreP1.setText(scorP1);
        scoreP2.setText(scorP2);
        String nbPiecesPB = Integer.toString(game.numberOfPieceOfPlayer(PlayerColor.BLACK));
        String nbPiecesPW = Integer.toString(game.numberOfPieceOfPlayer(PlayerColor.WHITE));
        nbPiecesP1.setText(nbPiecesPB);
        nbPiecesP2.setText(nbPiecesPW);
    }

    public void resetWindow() {
        scoreP1.setText("2");
        scoreP2.setText("2");
        nbPiecesP1.setText("2");
        nbPiecesP2.setText("2");
    }

}
