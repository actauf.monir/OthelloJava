package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Action;
import esi.atl.othello.monir.model.Model;
import esi.atl.othello.monir.model.PlayerColor;
import esi.atl.othello.monir.model.Position;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 * This class represents the right side of the scene.
 *
 * @author Monir Nasser
 */
public final class DataSide extends VBox {

    private GridPane dataPlayer1;
    private GridPane dataPlayer2;
    private final HBox hboxPlayers;
    private Text nameP1;
    private Text nameP2;
    private Text scoreP1;
    private Text scoreP2;
    private ActionTable dataTab;
    private ObservableList<Action> data;

    public DataSide(Model Game) {
        this.setPadding(new Insets(10));
        initGridPlayer1();
        initGridPlayer2();
        hboxPlayers = new HBox(10);
        hboxPlayers.setPadding(new Insets(5, 5, 5, 10));
        hboxPlayers.getChildren().addAll(dataPlayer1, dataPlayer2);
        initDataTab();
        this.getChildren().addAll(hboxPlayers, dataTab);
    }

    /**
     * This method initialize the gridpane who contains the data of the black
     * player.
     */
    void initGridPlayer1() {
        dataPlayer1 = new GridPane();
        dataPlayer1.setPadding(new Insets(5));
        dataPlayer1.setVgap(10);
        dataPlayer1.setHgap(10);
        dataPlayer1.setBackground(new Background(new BackgroundFill(Color.CADETBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
        dataPlayer1.setAlignment(Pos.CENTER);
        Text name1 = new Text("Name");
        Text pion1 = new Text("Pion");
        Text score1 = new Text("Score");
        nameP1 = new Text();
        scoreP1 = new Text("2");
        nameP1.setFont(Font.font(15));
        scoreP1.setFont(Font.font(15));
        name1.setFont(Font.font(20));
        pion1.setFont(Font.font(20));
        score1.setFont(Font.font(20));
        Circle cBlack = new Circle(3, Color.BLACK);
        GridPane.setHalignment(cBlack, HPos.CENTER);
        GridPane.setHalignment(nameP1, HPos.CENTER);
        GridPane.setHalignment(scoreP1, HPos.CENTER);
        dataPlayer1.add(name1, 0, 0);
        dataPlayer1.add(nameP1, 0, 1);
        dataPlayer1.add(pion1, 2, 0);
        dataPlayer1.add(cBlack, 2, 1);
        dataPlayer1.add(score1, 4, 0);
        dataPlayer1.add(scoreP1, 4, 1);
    }

    /**
     * This method initialize the gridpane who contains the data of the White
     * player.
     */
    void initGridPlayer2() {
        dataPlayer2 = new GridPane();
        dataPlayer2.setPadding(new Insets(5));
        dataPlayer2.setVgap(10);
        dataPlayer2.setHgap(10);
        dataPlayer2.setBackground(new Background(new BackgroundFill(Color.DARKOLIVEGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        Text name2 = new Text("Name");
        Text pion2 = new Text("Pion");
        Text score2 = new Text("Score");
        nameP2 = new Text();
        scoreP2 = new Text("2");
        nameP2.setFont(Font.font(15));
        scoreP2.setFont(Font.font(15));
        name2.setFont(Font.font(20));
        pion2.setFont(Font.font(20));
        score2.setFont(Font.font(20));
        Circle cWithe = new Circle(3, Color.WHITE);
        GridPane.setHalignment(cWithe, HPos.CENTER);
        GridPane.setHalignment(nameP2, HPos.CENTER);
        GridPane.setHalignment(scoreP2, HPos.CENTER);
        dataPlayer2.add(name2, 0, 0);
        dataPlayer2.add(nameP2, 0, 1);
        dataPlayer2.add(pion2, 1, 0);
        dataPlayer2.add(cWithe, 1, 1);
        dataPlayer2.add(score2, 2, 0);
        dataPlayer2.add(scoreP2, 2, 1);
    }

    /**
     * This method update the score of the 2 players.
     *
     * @param game The model game.
     */
    public void updateScore(Model game) {
        int score1 = game.getScore(game.getCurrentPlayer().getColor());
        this.scoreP1.setText(Integer.toString(score1));
        int score2 = game.getScore(game.getOpponentPlayer().getColor());
        this.scoreP2.setText(Integer.toString(score2));
    }

    /**
     * This method initialize the data table.
     */
    void initDataTab() {
        dataTab = new ActionTable();
        data = getInitialTableData();
        dataTab.setItems(data);

    }

    private ObservableList getInitialTableData() {
        List list = new ArrayList();
        list.add(new Action(0, "no one", "initialisation", null, 0));
        ObservableList data = FXCollections.observableList(list);
        return data;
    }

    /**
     * This method add the data.
     *
     * @param action the action do register.
     * @param game the model game.
     * @param dataside The dataSide.
     */
    public void updateTableView(Action action, Model game, DataSide dataside) {
        if (game.getCurrentPlayer().getColor() == PlayerColor.BLACK) {
            action.setName(dataside.getNameP2().getText());
        } else {
            action.setName(dataside.getNameP1().getText());
        }
        dataTab.getItems().add(action);
    }

    /**
     * This method clear the tableView.
     */
    public void resetTableView() {
        dataTab.getItems().clear();
    }

    /**
     * This method allows to get the name of the black player.
     *
     * @return The string name.
     */
    public Text getNameP1() {
        return nameP1;
    }

    /**
     * This method allows to get the name of the black player.
     *
     * @return The string name.
     */
    public Text getNameP2() {
        return nameP2;
    }

    public Text getScoreP1() {
        return scoreP1;
    }

    public Text getScoreP2() {
        return scoreP2;
    }

}
