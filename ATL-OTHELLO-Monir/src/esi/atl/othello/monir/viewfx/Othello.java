package esi.atl.othello.monir.viewfx;

import esi.atl.othello.monir.model.Model;
import esi.atl.othello.monir.model.PlayerColor;
import esi.atl.othello.monir.model.Position;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 * This class represents the board.
 *
 * @author Monir Nasser
 */
public final class Othello extends GridPane {

    private final Model game;
    private EventHandler<MouseEvent> eventClicked;
    private int x;
    private int y;

    public Othello(Model game) {
        this.setPadding(new Insets(5, 5, 5, 10));
        this.setHgap(1);
        this.setVgap(1);
        this.game = game;
        initBoard();
    }

    /**
     * This method initialize the board.
     */
    void initBoard() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Position pos = new Position(i, j);
                //Piece piece = game.getBoard().getPiece(pos);
                Square square = new Square(game, pos);
                if (game.getBoard().getSquare(pos).isBonus()) {
                    Rectangle rec = (Rectangle) square.getChildren().get(0);
                    rec.setFill(Color.CADETBLUE);
                }
                mouseEventClicked(square, i, j);
                this.add(square, j, i);
            }
        }
    }

    /**
     * this method update the board.
     */
    public void updateBoard() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square square = (Square) this.getChildren().get(i * 8 + j);
                Rectangle rec = (Rectangle) square.getChildren().get(0);
                if (!game.getBoard().isFree(new Position(i, j))) {
                    if (game.getCurrentPlayer().isComputer()) {
                        if (square.getChildren().size() == 1) {
                            Circle piece = square.createPiece(game.getBoard().getPiece(new Position(i, j)).getColor());
                            square.getChildren().add(piece);
                        }
                    }
                    Circle piece = (Circle) square.getChildren().get(1);
                    if (game.getBoard().getPiece(new Position(i, j)).getColor() == PlayerColor.BLACK) {
                        piece.setFill(Color.BLACK);
                    } else {
                        piece.setFill(Color.WHITE);
                    }
                } else {
                    if (square.getChildren().size() == 2) {
                        square.getChildren().remove(1);
                    }
                }
            }
        }
    }

    /**
     * This method create the event cliked on the square.
     *
     * @param square the square.
     * @param i The row.
     * @param j The col.
     */
    void mouseEventClicked(Square square, int i, int j) {
        eventClicked = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Position pos = new Position(i, j);
                if (game.getBoard().isFree(pos)) {
                    if (game.isValidMoveAllDirection(pos)) {
                        x = i;
                        y = j;
                        game.play(pos);
                    }
                }

            }
        };

        square.addEventHandler(MouseEvent.MOUSE_CLICKED, (eventClicked));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
