package esi.atl.othello.monir.model;

/**
 * This enum represents the color of the player and of the pawn.
 *
 * @author Monir Nasser
 */
public enum PlayerColor {
    BLACK,
    WHITE;
}
