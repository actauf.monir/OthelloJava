package esi.atl.othello.monir.model;

/**
 * This class represents the position of the pawn in the Board.
 *
 * @author Monir Nasser
 */
public class Position {

    private int row;
    private int column;

    /**
     * This methods construct the position whit 2 parameters.
     *
     * @param row The row of the position.
     * @param column The column of the position.
     */
    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * this method returns the position beside the current Position.
     *
     * @param direction the direction we want to go.
     */
    public void next(Direction direction) {
        this.row += direction.getRow();
        this.column += direction.getColumn();

    }

    /**
     * This method allows us to get the row of the position.
     *
     * @return The row.
     */
    public int getRow() {
        return row;
    }

    /**
     * This method allows us to get the column of the position.
     *
     * @return The column.
     */
    public int getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return "Position{" + "row=" + row + ", column=" + column + '}';
    }
    
}
