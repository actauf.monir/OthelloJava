package esi.atl.othello.monir.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents the Player.
 *
 * @author Monir Nasser
 */
public class Player {

    private PlayerColor color;
    private Boolean computer;
    private List<Piece> pieces;
    private int points = 2;

    /**
     * Allows to initialize the player with a Black color and an empty list of
     * pieces.
     */
    Player() {
        color = PlayerColor.BLACK;
        pieces = new ArrayList<>();
        points = 2;
    }

    /**
     * This method check if the color is null and throw a new exception.
     *
     * @param color the color of the piece.
     */
    Player(PlayerColor color) {
        if (color == null) {
            throw new NullPointerException("color is not defined");
        }
        computer = false;
        this.color = color;
        pieces = new ArrayList<>();
        points = 2;
    }

    /**
     * we can get the color of the object with this method
     *
     * @return the color
     */
    public PlayerColor getColor() {
        return color;
    }

    /**
     * This method allows us to add a piece in the list.
     *
     * @param piece The piece we ant to add.
     */
    void addPiece(Piece piece) {
        pieces.add(piece);
    }

    void addPoints(Piece piece) {
        this.points += piece.getValue();
    }

    void removePoints(Piece piece) {
        this.points -= piece.getValue();
    }

    /**
     * This method allows us to get the last piece in the list.
     *
     * @return The last piece of the list.
     */
    Piece getPiece() {
        return pieces.get(sizeList() - 1);
    }

    /**
     * This method allows us to get the size of the list.
     *
     * @return The size of the list.
     */
    public int sizeList() {
        return pieces.size();
    }

    /**
     * This method allows us to shuffle the list.
     */
    void shuffleList() {
        Collections.shuffle(pieces);
    }

    /**
     * This method allows us to remove the last pieces of the list.
     */
    Piece removePiece() {
        return pieces.remove(sizeList() - 1);
    }

    public int getPoints() {
        return points;
    }

    public Boolean isComputer() {
        return computer;
    }

    public void setComputer(Boolean computer) {
        this.computer = computer;
    }

    public void addBonus() {
        points +=3;
    }

    public void removeBonus() {
        points -= 3;
    }
}
