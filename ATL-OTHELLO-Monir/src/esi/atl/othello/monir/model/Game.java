package esi.atl.othello.monir.model;

import esi.atl.othello.util.Observable;
import esi.atl.othello.util.Observer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents the facade of the game.
 *
 * @author Monir Nasser
 */
public class Game implements Model, Observable {

    private Board board;
    private Player current;
    private Player opponent;
    private final int NB_PCS0 = 1;
    private final int NB_PCS1 = 20;
    private final int NB_PCS2 = 12;
    private final int NB_PCS3 = 1;
    private final int MAX_POINTS = (NB_PCS1 + NB_PCS2 * 2 + NB_PCS3 * 3) * 2;
    private final int width = 8;
    private final List<Observer> observers = new ArrayList<>();

    /**
     * This method construct the Game.
     */
    public Game() {
        this.board = null;
        this.current = new Player(PlayerColor.BLACK);
        this.opponent = new Player(PlayerColor.WHITE);

    }

    /**
     * This method initialize the game and pose 2 pieces black and 2 white.
     */
    @Override
    public void initialize() {
        board = new Board();
        board.put(new Piece(PlayerColor.BLACK, 1), new Position(3, 4));
        board.put(new Piece(PlayerColor.BLACK, 1), new Position(4, 3));
        board.put(new Piece(PlayerColor.WHITE, 1), new Position(3, 3));
        board.put(new Piece(PlayerColor.WHITE, 1), new Position(4, 4));

        for (int i = 0; i < NB_PCS2; i++) {
            current.addPiece(new Piece(PlayerColor.BLACK, 2));
            opponent.addPiece(new Piece(PlayerColor.WHITE, 2));
        }
        for (int i = 0; i < NB_PCS1 - 2; i++) {
            current.addPiece(new Piece(PlayerColor.BLACK, 1));
            opponent.addPiece(new Piece(PlayerColor.WHITE, 1));
        }

        current.addPiece(new Piece(PlayerColor.BLACK, 3));
        current.addPiece(new Piece(PlayerColor.BLACK, 0));
        opponent.addPiece(new Piece(PlayerColor.WHITE, 3));
        opponent.addPiece(new Piece(PlayerColor.WHITE, 0));

        current.shuffleList();
        opponent.shuffleList();
    }

    /**
     * This method start the game and check if the board is null or the game is
     * over.
     */
    @Override
    public void start() {
        if (board == null) {
            throw new IllegalStateException("board is not defined");
        }
        if (isOver()) {
            throw new IllegalStateException("board is over");
        }
    }

    /**
     * This method check if the game is over.
     *
     * @return A boolean.
     */
    @Override
    public boolean isOver() {
        return isBoardFull() || (!hasMoves2Players()
                && !hasMoves2Players());
    }

    /**
     * This method check if the board is full.
     *
     * @return true if is full.
     */
    boolean isBoardFull() { // public ?
        boolean isFull = true;
        if (current.getPoints() + opponent.getPoints() != MAX_POINTS) {
            isFull = false;
        }
        return isFull;
    }

    /**
     * This method put the piece at the position given in parameter.
     *
     * @param position Position.
     */
    @Override
    public void play(Position position) {
        if (position == null) {
            throw new IllegalArgumentException("the position is null");
        }
        if (isValidMoveAllDirection(position)) {
            if (board.getSquare(position).isBonus()) {
                current.addBonus();
            }
            board.put(current.getPiece(), position);
            current.addPoints(current.getPiece());
            current.removePiece();
            eatPiecesAllDirection(position);
        }
        swapPlayers();
        notifyObserver();

        if (current.isComputer()) {
            ArrayList<Position> allPos = allValidMoveAllDirection();
            int max = allPos.size() - 1;
            int rand = (int) (Math.random() * max);
            if (!allPos.isEmpty()) {
                while (!board.isFree(allPos.get(rand))) {
                    rand = (int) (Math.random() * max);
                }
                System.out.println(allPos);
                System.out.println(rand);
                board.put(current.getPiece(), allPos.get(rand));
                current.addPoints(current.getPiece());
                current.removePiece();
                eatPiecesAllDirection(position);
            }
            notifyObserver();
            swapPlayers();
        }

    }

    /**
     * This method returns us the value of black and white pieces in the board.
     *
     * @param color the player color.
     * @return A tab of integer.
     */
    @Override
    public int getScore(PlayerColor color) {
        int points = -1;
        if (color == current.getColor()) {
            points = current.getPoints();
        } else {
            points = opponent.getPoints();
        }
        return points;
    }

    /**
     * This method check if the 2 players has moves.
     *
     * @return true or false.
     */
    @Override
    public boolean hasMoves2Players() {
        boolean hasMoves = false;
        int row = 0;
        while (!hasMoves && row < board.getNB_ROW()) {
            int col = 0;
            while (!hasMoves && col < board.getNB_COLUMN()) {
                Position position = new Position(row, col);
                if (board.isFree(position)) {
                    if (isValidMoveAllDirection(position)) {
                        hasMoves = true;
                    }
                }
                col++;
            }
            row++;
        }
        if (!hasMoves) {
            swapPlayers();
        }
        return hasMoves;
    }

    /**
     * This method verify if the move is valid in the direction given in
     * parameter.
     *
     * @param position The position start
     * @param direction The direction we will go.
     * @return True if is valid.
     */
    boolean isValidMove(Position position, Direction direction) {
        boolean hasMove = false;
        Position pos = new Position(position.getRow(), position.getColumn());
        pos.next(direction);
        if (board.isInside(pos)) {
            if (!board.isFree(pos)) {
                if (board.getPiece(pos).getColor() != current.getColor()) {
                    while (!hasMove && board.isInside(pos) && !board.isFree(pos)) {
                        if (board.getPiece(pos).getColor() == current.getColor()) {
                            hasMove = true;
                        }
                        pos.next(direction);
                    }
                }
            }
        }
        return hasMove;
    }

    /**
     * This method verify the move in all directions.
     *
     * @param position The position star.
     * @return true if one direction is valid.
     */
    @Override
    public boolean isValidMoveAllDirection(Position position) {
        boolean hasMoves = false;
        List<Direction> listDirection = Arrays.asList(Direction.UP,
                Direction.DOWN, Direction.UPLEFT,
                Direction.LEFT, Direction.DOWNLEFT,
                Direction.RIGHT, Direction.DOWNRIGTH,
                Direction.UPRIGTH);
        int i = 0;
        while (!hasMoves && i < listDirection.size()) {
            if (isValidMove(position, listDirection.get(i))) {
                hasMoves = true;
            }
            i++;
        }
        return hasMoves;
    }

    /**
     * This method verify the move in all directions.
     *
     * @return the list of valid position.
     */
    public ArrayList<Position> allValidMoveAllDirection() {
        ArrayList<Position> validPos = new ArrayList<>();
        List<Direction> listDirection = Arrays.asList(Direction.UP,
                Direction.DOWN, Direction.UPLEFT,
                Direction.LEFT, Direction.DOWNLEFT,
                Direction.RIGHT, Direction.DOWNRIGTH,
                Direction.UPRIGTH);

        for (int j = 0; j < 8; j++) {
            for (int k = 0; k < 8; k++) {
                Position position = new Position(j, k);
                Position pos = new Position(j, k);
                int i = 0;
                while (i < listDirection.size()) {
                    if (isValidMove(position, listDirection.get(i))) {
                        validPos.add(pos);
                    }
                    i++;
                }
            }
        }

        return validPos;
    }

    /**
     * This method change the color of pieces who are eat.
     *
     * @param position The position
     * @param direction The direction we eat.
     */
    void eatPieces(Position position, Direction direction) {
        Position pos = new Position(position.getRow(), position.getColumn());
        pos.next(direction);
        while (board.getPiece(pos).getColor() != current.getColor()) {
            if (board.getSquare(position).isBonus()) {
                current.addBonus();
                opponent.removeBonus();
            }
            opponent.removePoints(board.getPiece(pos));
            board.getPiece(pos).swapColor();
            current.addPoints(board.getPiece(pos));
            pos.next(direction);
        }
    }

    /**
     * This method change the color in all direction who are valid.
     *
     * @param position The position start.
     */
    void eatPiecesAllDirection(Position position) {
        List<Direction> listDirection = Arrays.asList(Direction.UP,
                Direction.DOWN, Direction.UPLEFT,
                Direction.LEFT, Direction.DOWNLEFT,
                Direction.RIGHT, Direction.DOWNRIGTH,
                Direction.UPRIGTH);

        int i = 0;
        while (i < listDirection.size()) {
            if (isValidMove(position, listDirection.get(i))) {
                eatPieces(position, listDirection.get(i));
            }
            i++;
        }
    }

    /**
     * This method change the current player.
     */
    @Override
    public void swapPlayers() {
        Player playerswitch = current;
        current = opponent;
        opponent = playerswitch;
        //notifyObserver();
    }

    /**
     * This method calculate the number of the piece of the color in the board.
     *
     * @param color the color white or black.
     * @return the number of the piece.
     */
    @Override
    public int numberOfPieceOfPlayer(PlayerColor color) {
        int nb = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                if (!board.isFree(new Position(i, j))) {
                    if (board.getPieces()[i][j].getPiece().getColor() == color) {
                        nb++;
                    }
                }
            }
        }
        return nb;
    }

    /**
     * This method returns the winner.
     *
     * @return the player winner;
     */
    @Override
    public Player getWinner() {
        Player winner;
        if (current.getPoints() > opponent.getPoints()) {
            winner = current;
        } else {
            winner = opponent;
        }
        return winner;
    }

    /**
     * This method reset the game.
     */
    @Override
    public void resetGame() {
        board = null;
        current = new Player(PlayerColor.BLACK);
        opponent = new Player(PlayerColor.WHITE);
        initialize();
        notifyObserver();
    }

    /**
     * This method allows us to get the player current.
     *
     * @return The current.
     */
    @Override
    public Player getCurrentPlayer() {
        return current;
    }

    /**
     * This method allows us to get the player opponent.
     *
     * @return The opponent.
     */
    @Override
    public Player getOpponentPlayer() {
        return opponent;
    }

    /**
     * this method allows us to get the board.
     *
     * @return The board.
     */
    @Override
    public Board getBoard() {
        return board;
    }

    /**
     * This method just put the piece at the position for the tests.
     *
     * @param position The position in the boeard.
     */
    void putPiece2(Position position, Piece piece) {
        if (position == null) {
            throw new IllegalArgumentException("the position is null");
        }
        this.board.put(piece, position);
    }

    /**
     * This method add the observer.
     *
     * @param obs The observer to add.
     */
    @Override
    public void addObserver(Observer obs) {
        this.observers.add(obs);
    }

    /**
     * This method remove the observer.
     *
     * @param obs The observer to remove.
     */
    @Override
    public void removeObserver(Observer obs) {
        this.observers.remove(obs);
    }

    /**
     * This method notify the observers.
     */
    @Override
    public void notifyObserver() {
        observers.forEach((observer) -> {
            observer.update();
        });
    }

}
