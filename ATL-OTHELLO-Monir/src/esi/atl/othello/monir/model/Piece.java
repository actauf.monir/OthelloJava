package esi.atl.othello.monir.model;

/**
 * This class represents the pawn.
 *
 * @author Monir Nasser
 */
public class Piece {

    private PlayerColor color;
    private int value;

    /**
     * This method construct the Pawn.
     *
     * @param color The color of the piece.
     * @param value The value of the piece.
     */
    Piece(PlayerColor color, int value) {
        if (color == null) {
            throw new IllegalArgumentException("The coolor is null");
        }
        if (value < 0 || value > 3) {
            throw new IllegalArgumentException("The value is not accepted");
        }
        this.color = color;
        this.value = value;
    }

    /**
     * This method changes the color of the piece.
     */
    void swapColor() {
        if (this.color == PlayerColor.BLACK) {
            this.color = PlayerColor.WHITE;
        } else {
            this.color = PlayerColor.BLACK;
        }
    }

    /**
     * This methods allows us to get the color of the pawn.
     *
     * @return The color.
     */
    public PlayerColor getColor() {
        return color;
    }

    /**
     * This method allows us to get the value.
     *
     * @return A Value.
     */
    public int getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Piece other = (Piece) obj;
        if (this.value != other.value) {
            return false;
        }
        if (this.color != other.color) {
            return false;
        }
        return true;
    }

}
