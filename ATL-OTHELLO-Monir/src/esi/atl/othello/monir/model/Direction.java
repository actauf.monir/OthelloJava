package esi.atl.othello.monir.model;

/**
 * This class represents the direction.
 *
 * @author Monir Nasser
 */
public enum Direction {
    /**
     * The direction UP.
     */
    UP(-1, 0),
    /**
     * The direction UP.
     */
    DOWN(1, 0),
    /**
     * The direction LEFT.
     */
    LEFT(0, -1),
    /**
     * The direction RIGTH.
     */
    RIGHT(0, 1),
    /**
     * The direction UPRIGTH.
     */
    UPRIGTH(-1, 1),
    /**
     * The direction UPLEFT.
     */
    UPLEFT(-1, -1),
    /**
     * The direction DOWNRIGTH.
     */
    DOWNRIGTH(1, 1),
    /**
     * The direction DOWNLEFT.
     */
    DOWNLEFT(1, -1);

    private final int row;
    private final int column;

    /**
     * This method constrcut the direction with the row and column in parameter.
     *
     * @param row The row of the direction.
     * @param column The column of the direction.
     */
    private Direction(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * This methods allows us to get the row of the direction.
     *
     * @return The row.
     */
    public int getRow() {
        return row;
    }

    /**
     * This methods allows us to get the column of the direction.
     *
     * @return The column.
     */
    public int getColumn() {
        return column;
    }

}
