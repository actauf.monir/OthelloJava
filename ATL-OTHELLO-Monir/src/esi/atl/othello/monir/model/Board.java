package esi.atl.othello.monir.model;

/**
 * This class represents the Board.
 *
 * @author Monir Nasser
 */
public class Board {

    private final Square[][] pieces;
    private final int NB_ROW = 8;
    private final int NB_COLUMN = 8;

    /**
     * This method construct the board.
     */
    public Board() {
        pieces = new Square[NB_ROW][NB_COLUMN];
        for (int j = 0; j < 8; j++) {
            for (int k = 0; k < 8; k++) {
                pieces[j][k] = new Square();
            }
        }
        int max = 7;
        int range = max;
        int i = 0;
        while (i < 3) {
            int row = (int) (Math.random() * range);
            int col = (int) (Math.random() * range);
            if ((row != 3 && col != 4) || (row != 3 && col != 3)
                    || (row != 4 && col != 4) || (row != 4 && col != 3)) {
                pieces[row][col] = new Square(SquareType.BONUS);
                i++;
            }
        }
    }

    /**
     * This method check if the position is inside the board.
     *
     * @param position The position.
     * @return A Boolean.
     */
    public boolean isInside(Position position) {
        return (pieces.length > position.getRow()
                && position.getRow() >= 0)
                && (pieces[0].length > position.getColumn()
                && position.getColumn() >= 0);
    }

    /**
     * This method check if the square is free or not.
     *
     * @param position The position.
     * @return A boolean
     */
    public boolean isFree(Position position) {
        if (!isInside(position)) {
            throw new IllegalArgumentException("the position is not inside of the board");
        }
        return pieces[position.getRow()][position.getColumn()].isFree();
    }

    /**
     * this method put a pieces in the board in a given position and check if
     * the position is inside or not and if the piece is null
     *
     * @param piece
     * @param position
     */
    void put(Piece piece, Position position) {
        if (isInside(position) == false) {
            throw new IllegalArgumentException("the position is outside of the board.");
        }
        if (piece == null) {
            throw new IllegalArgumentException("the piece is null");
        }
        if (!isFree(position)) {
            throw new IllegalArgumentException("The position is not free");
        }
        this.pieces[position.getRow()][position.getColumn()].put(piece);;
    }

    /**
     * This method allows us to get he piece at the position given in paramater.
     *
     * @param position The position we need the piece.
     * @return The piece at the position.
     */
    public Piece getPiece(Position position) {
        return pieces[position.getRow()][position.getColumn()].getPiece();
    }

    /**
     * This method allows us to get the tab of pieces.
     *
     * @return The tab of pieces.
     */
    Square[][] getPieces() {
        return pieces;
    }

    /**
     * This method allows us to get the number row of board.
     *
     * @return An int.
     */
    public int getNB_ROW() {
        return NB_ROW;
    }

    /**
     * This method allows us to get the number column of board.
     *
     * @return An int.
     */
    public int getNB_COLUMN() {
        return NB_COLUMN;
    }

    public Square getSquare(Position position) {
        return this.pieces[position.getRow()][position.getColumn()];
    }
}
