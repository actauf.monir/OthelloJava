package esi.atl.othello.monir.model;

/**
 * This class represents the square in the board.
 *
 * @author Monir Nasser
 */
public class Square {

    private Piece piece;
    private SquareType type;

    public Square() {
        this.piece = null;
        type = SquareType.NORMAL;
    }

    public Square(SquareType type) {
        piece = null;
        this.type = type;
    }

    public Piece getPiece() {
        return piece;
    }

    /**
     * this check if the place is free or not
     *
     * @return true if is nothing or false if it's full
     */
    public boolean isFree() {
        return this.piece == null;
    }

    /**
     * this method put the piece at the place;
     *
     * @param piece
     */
    public void put(Piece piece) {
        if (piece == null) {
            throw new IllegalArgumentException("piece can't be null");
        }
        if (!isFree()) {
            throw new IllegalArgumentException("the square is full");
        }
        this.piece = piece;
    }

    public SquareType getType() {
        return type;
    }

    public boolean isBonus() {
        return this.getType() == SquareType.BONUS;
    }

    void setType(SquareType type) {
        this.type = type;
    }

}
