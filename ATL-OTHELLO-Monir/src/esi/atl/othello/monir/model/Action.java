package esi.atl.othello.monir.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * This class represents the action of one play.
 *
 * @author Monir Nasser
 */
public class Action {

    private Integer id;
    private String name;
    private String action;
    private String position;
    private Integer prise;

    /**
     * This method initialize the action.
     *
     * @param id The id of the action.
     * @param name The name of the player.
     * @param position The position.
     * @param prise The number of the piece.
     */
    public Action(Integer id, String name, String action, String position, Integer prise) {
        this.id = id;
        this.name = name;
        this.action = action;
        this.position = position;
        this.prise = prise;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAction() {
        return action;
    }

    public String getPosition() {
        return position;
    }

    public Integer getPrise() {
        return prise;
    }

    public void setName(String name) {
        this.name = name;
    }
}
