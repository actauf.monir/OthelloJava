package esi.atl.othello.monir.model;

/**
 * This represents the type of the square
 *
 * @author Monir Nasser
 */
public enum SquareType {
    NORMAL,
    BONUS;

}
