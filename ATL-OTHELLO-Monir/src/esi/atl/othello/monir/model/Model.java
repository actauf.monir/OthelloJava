package esi.atl.othello.monir.model;

/**
 * This class represents the Model.
 *
 * @author Monir Nasser
 */
public interface Model {

    /**
     * Initializes the Othello game with a default board.
     */
    void initialize();

    /**
     * Checks if a match can start.
     *
     * @throws IllegalStateException if no board is set.
     * @throws IllegalStateException if the board set is incomplete.
     */
    void start();

    /**
     * Declares if it's the end of the game.
     *
     * @return true if it's the end of the game.
     */
    boolean isOver();

    /**
     * Returns the board.
     *
     * @return the board.
     */
    Board getBoard();

    /**
     * This method put the piece at the position given in parameter.
     *
     * @param position Position.
     */
    void play(Position position);

    /**
     * This method check if the move is valid.
     *
     * @param position the position
     * @return true or false.
     */
    boolean isValidMoveAllDirection(Position position);

    /**
     * This method check if the player has moves.
     *
     * @return true or false.
     */
    boolean hasMoves2Players();

    /**
     * This method allows us to get the player current.
     *
     * @return The current.
     */
    Player getCurrentPlayer();

    /**
     * This method allows us to get the player opponent.
     *
     * @return The opponent.
     */
    Player getOpponentPlayer();

    /**
     * This method swap the players
     *
     */
    void swapPlayers();

    /**
     * This method returns the score of the player.
     *
     * @param color the color of the player.
     * @return the score.
     */
    int getScore(PlayerColor color);

    /**
     * This method calculate the number of the piece of the color in the board.
     *
     * @param color the color white or black.
     * @return the number of the piece.
     */
    int numberOfPieceOfPlayer(PlayerColor color);

    /**
     * This method returns the winner.
     *
     * @return the player winner;
     */
    Player getWinner();

    /**
     * This method reset the game.
     */
    void resetGame();
}
