package esi.atl.othello.util;

/**
 * this interface represents the Observable.
 *
 * @author Monir Nasser
 */
public interface Observable {

    /**
     * This method add the observer at the list.
     *
     * @param obs an Observer.
     */
    public void addObserver(Observer obs);

    /**
     * This method remove the observer at the list.
     *
     * @param obs An observer.
     */
    public void removeObserver(Observer obs);

    /**
     * This method notify the observer that state changed.
     */
    public void notifyObserver();
}
