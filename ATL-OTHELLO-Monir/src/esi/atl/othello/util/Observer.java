package esi.atl.othello.util;

/**
 * This interface represents the observer.
 *
 * @author Monir Nasser
 */
public interface Observer {

    /**
     * This method update the code when the observable has change state.
     */
    public void update();
}
