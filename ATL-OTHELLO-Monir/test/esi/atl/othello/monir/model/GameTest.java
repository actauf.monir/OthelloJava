package esi.atl.othello.monir.model;

import esi.atl.othello.monir.view.View;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class represents the test of the class game.
 *
 * @author Monir Nasser
 */
public class GameTest {

    public final Piece[][] defaulBoard = {
        {null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null},
        {null, null, null, new Piece(PlayerColor.WHITE, 1), new Piece(PlayerColor.BLACK, 1), null, null, null},
        {null, null, null, new Piece(PlayerColor.BLACK, 1), new Piece(PlayerColor.WHITE, 1), null, null, null},
        {null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null}};

    public GameTest() {
    }

    /**
     * Test of initialize method, of class Game.
     */
    @Test
    public void testInitialize() {
        System.out.println("initialize");
        Game instance = new Game();
        instance.initialize();
        assertArrayEquals(defaulBoard, instance.getBoard().getPieces());
    }

    /**
     * Test of start method, of class Game.
     */
    @Test(expected = IllegalStateException.class)
    public void testStartWhenBoardIsNotDefined() {
        System.out.println("start");
        Game instance = new Game();
        instance.start();
    }

    /**
     * Test of start method, of class Game.
     */
    @Test(expected = IllegalStateException.class)
    public void testStartWhenBoardIsOver() {
        System.out.println("start");
        Game instance = new Game();
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Piece piece2 = new Piece(PlayerColor.BLACK, 1);
        for (int i = 0; i < 20; i++) {
            instance.getCurrentPlayer().addPoints(piece);
        }
        instance.getCurrentPlayer().addPoints(piece2);
        for (int i = 0; i < 20; i++) {
            instance.getOpponentPlayer().addPoints(piece);
        }
        instance.getOpponentPlayer().addPoints(piece2);
        instance.start();
    }

    /**
     * Test of isOver method, of class Game.
     */
    @Test
    public void testIsOver() {
        System.out.println("isOver");
        Game instance = new Game();
        instance.initialize();
        boolean expResult = false;
        boolean result = instance.isOver();
        assertEquals(expResult, result);
    }

    /**
     * Test of isOver method, of class Game.
     */
    @Test
    public void testIsOverWhenBoardIsFull() {
        System.out.println("isOver");
        Game instance = new Game();
        boolean expResult = true;
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Piece piece2 = new Piece(PlayerColor.BLACK, 1);
        for (int i = 0; i < 20; i++) {
            instance.getCurrentPlayer().addPoints(piece);
        }
        instance.getCurrentPlayer().addPoints(piece2);
        for (int i = 0; i < 20; i++) {
            instance.getOpponentPlayer().addPoints(piece);
        }
        instance.getOpponentPlayer().addPoints(piece2);
        boolean result = instance.isOver();
        assertEquals(expResult, result);
    }

    /**
     * Test of isOver method, of class Game.
     */
    @Test
    public void testIsOverWhenCurrentHasNotMoves() {
        System.out.println("isOver");
        Game instance = new Game();
        instance.initialize();
        boolean expResult = true;
        for (int row = 0; row < instance.getBoard().getNB_ROW(); row++) {
            instance.putPiece2(new Position(row, 0), new Piece(PlayerColor.WHITE, 1));
            if (row > 0) {
                instance.putPiece2(new Position(row, 1), new Piece(PlayerColor.WHITE, 1));
            }

        }
        int row2 = 2;
        for (int col = 2; col < instance.getBoard().getNB_COLUMN(); col++) {
            row2 = 2;
            instance.putPiece2(new Position(0, col), new Piece(PlayerColor.BLACK, 1));
            instance.putPiece2(new Position(1, col), new Piece(PlayerColor.BLACK, 1));
            if (col > 3) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 4) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 4) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 5) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 6) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
            }
        }
        instance.putPiece2(new Position(2, 3), new Piece(PlayerColor.BLACK, 1));
        for (int col = 2; col < instance.getBoard().getNB_COLUMN(); col++) {
            instance.putPiece2(new Position(7, col), new Piece(PlayerColor.WHITE, 1));
        }
        instance.putPiece2(new Position(3, 2), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(5, 3), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(5, 4), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(6, 5), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(2, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(4, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(5, 5), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(5, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 3), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 4), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 6), new Piece(PlayerColor.WHITE, 1));
        boolean result = instance.isOver();
        assertEquals(expResult, result);
    }

    @Test
    public void testHasMovesOpponentPlayer() {
        Game instance = new Game();
        View view = new View();
        instance.initialize();
        boolean expResult = true;
        int col3 = 1;
        for (int row = 0; row < instance.getBoard().getNB_ROW(); row++) {
            if (row == 1 && col3 == 1) {
                instance.putPiece2(new Position(row, col3), new Piece(PlayerColor.BLACK, 1));
                col3++;
            }
            instance.putPiece2(new Position(row, 0), new Piece(PlayerColor.WHITE, 1));
            if (row > 1) {
                instance.putPiece2(new Position(row, 1), new Piece(PlayerColor.WHITE, 1));
            }

        }
        int row2 = 2;
        for (int col = 2; col < instance.getBoard().getNB_COLUMN(); col++) {
            row2 = 2;
            instance.putPiece2(new Position(0, col), new Piece(PlayerColor.BLACK, 1));
            if (col > 3) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 4) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 4) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 5) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
                row2++;
            }
            if (col > 6) {
                instance.putPiece2(new Position(row2, col), new Piece(PlayerColor.BLACK, 1));
            }
        }
        instance.putPiece2(new Position(2, 3), new Piece(PlayerColor.BLACK, 1));
        for (int col = 2; col < instance.getBoard().getNB_COLUMN(); col++) {
            instance.putPiece2(new Position(7, col), new Piece(PlayerColor.WHITE, 1));
        }
        instance.putPiece2(new Position(3, 2), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(5, 3), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(5, 4), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(6, 5), new Piece(PlayerColor.BLACK, 1));
        instance.putPiece2(new Position(2, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(4, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(5, 5), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(5, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 2), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 3), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 4), new Piece(PlayerColor.WHITE, 1));
        instance.putPiece2(new Position(6, 6), new Piece(PlayerColor.WHITE, 1));
        boolean result = (instance.hasMoves2Players()
                && instance.hasMoves2Players());
        view.displayBoard(instance.getBoard());
        assertEquals(expResult, result);
    }

    /**
     * Test of isBoardFull method, of class Game.
     */
    @Test
    public void testIsBoardFullWhenFalse() {
        System.out.println("isBoardFull");
        Game instance = new Game();
        boolean expResult = false;
        boolean result = instance.isBoardFull();
        assertEquals(expResult, result);
    }

    /**
     * Test of play method, of class Game.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPutPieceWhenPositionIsNull() {
        System.out.println("putPiece");
        Position position = null;
        Game instance = new Game();
        instance.initialize();
        instance.play(position);
    }

    /**
     * Test of play method, of class Game.
     */
    @Test
    public void testPutPieceWhenWeCanEat() {
        System.out.println("putPiece");
        Position position = new Position(4, 5);
        Game instance = new Game();
        instance.initialize();
        Piece expResult = instance.getCurrentPlayer().getPiece();
        instance.play(position);
        Piece result = instance.getBoard().getPiece(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of getScore method, of class Game.
     */
    @Test
    public void testValuePieces() {
        System.out.println("valuePieces");
        Game instance = new Game();
        instance.initialize();
        int expResult = 2;
        int result = instance.getScore(instance.getCurrentPlayer().getColor());
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidMove method, of class Game.
     */
    @Test
    public void testIsValidMoveWhenTrue() {
        System.out.println("isValidMove");
        Position position = new Position(4, 5);
        Direction direction = Direction.LEFT;
        Game instance = new Game();
        instance.initialize();
        boolean expResult = true;
        boolean result = instance.isValidMove(position, direction);
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidMove method, of class Game.
     */
    @Test
    public void testIsValidMoveWhenPosIsNotInside() {
        System.out.println("isValidMove");
        Position position = new Position(10, 5);
        Direction direction = Direction.LEFT;
        Game instance = new Game();
        instance.initialize();
        boolean expResult = false;
        boolean result = instance.isValidMove(position, direction);
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidMove method, of class Game.
     */
    @Test
    public void testIsValidMoveWhenFalse() {
        System.out.println("isValidMove");
        Position position = new Position(4, 7);
        Direction direction = Direction.LEFT;
        Game instance = new Game();
        instance.initialize();
        boolean expResult = false;
        boolean result = instance.isValidMove(position, direction);
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidMoveAllDirection method, of class Game.
     */
    @Test
    public void testIsValidMoveAllDirectionWhenTrue() {
        System.out.println("isValidMoveAllDirection");
        Position position = new Position(4, 5);
        Game instance = new Game();
        instance.initialize();
        boolean expResult = true;
        boolean result = instance.isValidMoveAllDirection(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidMoveAllDirection method, of class Game.
     */
    @Test
    public void testIsValidMoveAllDirectionWhenFalse() {
        System.out.println("isValidMoveAllDirection");
        Position position = new Position(6, 6);
        Game instance = new Game();
        instance.initialize();
        boolean expResult = false;
        boolean result = instance.isValidMoveAllDirection(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of eatPieces method, of class Game.
     */
    @Test
    public void testEatPieces() {
        System.out.println("eatPieces");
        Position position = new Position(4, 5);
        Direction direction = Direction.LEFT;
        Game instance = new Game();
        instance.initialize();
        PlayerColor expResult = instance.getCurrentPlayer().getColor();
        instance.play(position);
        position.next(direction);
        PlayerColor result = instance.getBoard().getPiece(position).getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of eatPiecesAllDirection method, of class Game.
     */
    @Test
    public void testEatPiecesAllDirection() {
        System.out.println("eatPiecesAllDirection");
        Position position = new Position(4, 5);
        Game instance = new Game();
        instance.initialize();
        instance.eatPiecesAllDirection(position);
    }

    /**
     * Test of getCurrentPlayer method, of class Game.
     */
    @Test
    public void testGetCurrentWhenIsBlack() {
        System.out.println("getCurrent");
        Game instance = new Game();
        instance.initialize();
        PlayerColor expResult = PlayerColor.BLACK;
        PlayerColor result = instance.getCurrentPlayer().getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpponentPlayer method, of class Game.
     */
    @Test
    public void testGetOpponent() {
        System.out.println("getOpponent");
        Game instance = new Game();
        instance.initialize();
        PlayerColor expResult = PlayerColor.WHITE;
        PlayerColor result = instance.getOpponentPlayer().getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoard method, of class Game.
     */
    @Test
    public void testGetBoard() {
        System.out.println("getBoard");
        Game instance = new Game();
        instance.initialize();
        assertArrayEquals(defaulBoard, instance.getBoard().getPieces());
    }

    /**
     * Test of getBoard method, of class Game.
     */
    @Test
    public void testNumberOfPieceOfPlayer() {
        System.out.println("testNumberOfPieceOfPlayer");
        Game instance = new Game();
        instance.initialize();
        double expResult = 2.0;
        double result = instance.numberOfPieceOfPlayer(PlayerColor.BLACK);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getBoard method, of class Game.
     */
    @Test
    public void testPointsWithBonus() {
        System.out.println("testNumberOfPieceOfPlayer");
        Game instance = new Game();
        instance.initialize();
        instance.getBoard().getSquare(new Position(3, 2)).setType(SquareType.BONUS);
        instance.play(new Position(3, 2));
        int expResult = 7;
        int result = instance.getScore(PlayerColor.BLACK);
        assertEquals(expResult, result, 0.0);
    }
}
