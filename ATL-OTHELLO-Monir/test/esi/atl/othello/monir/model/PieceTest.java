package esi.atl.othello.monir.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class test the class Piece.
 *
 * @author Monir Nasser
 */
public class PieceTest {

    public PieceTest() {
    }

    /**
     * Test of swapColor method, of class Piece.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructWhenColorIsNull() {
        System.out.println("swapColor");
        Piece instance = new Piece(null, 1);
    }

    /**
     * Test of swapColor method, of class Piece.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructWhenValueIsToMuch() {
        System.out.println("swapColor");
        Piece instance = new Piece(PlayerColor.BLACK, 5);
    }

    /**
     * Test of swapColor method, of class Piece.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructWhenValueIsNotEnough() {
        System.out.println("swapColor");
        Piece instance = new Piece(PlayerColor.BLACK, -2);
    }

    /**
     * Test of swapColor method, of class Piece.
     */
    @Test
    public void testSwapColorBlackToWhite() {
        System.out.println("swapColor");
        Piece instance = new Piece(PlayerColor.BLACK, 1);
        instance.swapColor();
        PlayerColor expResult = PlayerColor.WHITE;
        PlayerColor result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of swapColor method, of class Piece.
     */
    @Test
    public void testSwapColorWhiteToBlack() {
        System.out.println("swapColor");
        Piece instance = new Piece(PlayerColor.WHITE, 1);
        instance.swapColor();
        PlayerColor expResult = PlayerColor.BLACK;
        PlayerColor result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getColor method, of class Piece.
     */
    @Test
    public void testGetColorBlack() {
        System.out.println("getColor");
        Piece instance = new Piece(PlayerColor.BLACK, 1);
        PlayerColor expResult = PlayerColor.BLACK;
        PlayerColor result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getColor method, of class Piece.
     */
    @Test
    public void testGetColorWhite() {
        System.out.println("getColor");
        Piece instance = new Piece(PlayerColor.WHITE, 1);
        PlayerColor expResult = PlayerColor.WHITE;
        PlayerColor result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getValue method, of class Piece.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        Piece instance = new Piece(PlayerColor.BLACK, 2);
        int expResult = 2;
        int result = instance.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of getValue method, of class Piece.
     */
    @Test
    public void testEqualsWhenObjectIsNull() {
        System.out.println("getValue");
        Piece instance = new Piece(PlayerColor.BLACK, 2);
        boolean expResult = false;
        boolean result = instance.equals(null);
        assertEquals(expResult, result);
    }
    /**
     * Test of getValue method, of class Piece.
     */
    @Test
    public void testEqualsWhenColorIsDifferent() {
        System.out.println("getValue");
        Piece instance = new Piece(PlayerColor.BLACK, 2);
        Piece piece = new Piece(PlayerColor.WHITE, 2);
        boolean expResult = false;
        boolean result = instance.equals(null);
        assertEquals(expResult, result);
    }
}
