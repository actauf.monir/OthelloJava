package esi.atl.othello.monir.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class represents the test of class Position.
 *
 * @author Monir Nasser
 */
public class PositionTest {

    public PositionTest() {
    }

    @Test
    public void nextUp() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.UP);
        int row = 1;
        int col = 3;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextUpLeft() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.UPLEFT);
        int row = 1;
        int col = 2;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextUpRigth() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.UPRIGTH);
        int row = 1;
        int col = 4;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextDown() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.DOWN);
        int row = 3;
        int col = 3;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextDownLeft() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.DOWNLEFT);
        int row = 3;
        int col = 2;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextDownRigth() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.DOWNRIGTH);
        int row = 3;
        int col = 4;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextLeft() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.LEFT);
        int row = 2;
        int col = 2;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    @Test
    public void nextRight() {
        Position position1 = new Position(2, 3);
        position1.next(Direction.RIGHT);
        int row = 2;
        int col = 4;
        assertEquals(row, position1.getRow());
        assertEquals(col, position1.getColumn());
    }

    /**
     * Test of getRow method, of class Position.
     */
    @Test
    public void testGetRow() {
        System.out.println("getRow");
        Position instance = new Position(5, 3);
        int expResult = 5;
        int result = instance.getRow();
        assertEquals(expResult, result);
    }

    /**
     * Test of getColumn method, of class Position.
     */
    @Test
    public void testGetColumn() {
        System.out.println("getColumn");
        Position instance = new Position(5, 3);
        int expResult = 3;
        int result = instance.getColumn();
        assertEquals(expResult, result);
    }

}
