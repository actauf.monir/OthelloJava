package esi.atl.othello.monir.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class test the class Player.
 *
 * @author Moni Nasser
 */
public class PlayerTest {

    public PlayerTest() {
    }

    /**
     * Test of getColor method, of class Player.
     */
    @Test(expected = NullPointerException.class)
    public void testConstructorWhenColorIsNull() {
        System.out.println("getColor");
        Player instance = new Player(null);
    }

    /**
     * Test of getColor method, of class Player.
     */
    @Test
    public void testGetColor() {
        System.out.println("getColor");
        Player instance = new Player();
        PlayerColor expResult = PlayerColor.BLACK;
        PlayerColor result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of addPiece method, of class Player.
     */
    @Test
    public void testAddPiece() {
        System.out.println("addPiece");
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Player instance = new Player();
        instance.addPiece(piece);
        assertEquals(piece, instance.getPiece());
    }

    /**
     * Test of getPiece method, of class Player.
     */
    @Test
    public void testGetPiece() {
        System.out.println("getPiece");
        Player instance = new Player();
        Piece expResult = new Piece(PlayerColor.BLACK, 1);
        instance.addPiece(expResult);
        Piece result = instance.getPiece();
        assertEquals(expResult, result);
    }

    /**
     * Test of sizeList method, of class Player.
     */
    @Test
    public void testSizeList() {
        System.out.println("sizeList");
        Player instance = new Player();
        int expResult = 0;
        int result = instance.sizeList();
        assertEquals(expResult, result);

    }

    /**
     * Test of shuffleList method, of class Player.
     */
    @Test
    public void testShuffleList() {
        System.out.println("shuffleList");
        Player instance = new Player();
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Piece piece2 = new Piece(PlayerColor.BLACK, 1);
        Piece piece3 = new Piece(PlayerColor.BLACK, 3);
        instance.addPiece(piece);
        instance.addPiece(piece2);
        instance.addPiece(piece3);
        instance.shuffleList();
        assertNotEquals(piece3, instance.getPiece());
    }

    /**
     * Test of removePiece method, of class Player.
     */
    @Test
    public void testRemovePiece() {
        System.out.println("removePiece");
        Player instance = new Player();
        Piece piece2 = new Piece(PlayerColor.BLACK, 1);
        instance.addPiece(piece2);
        Piece piece3 = new Piece(PlayerColor.BLACK, 1);
        instance.addPiece(piece3);
        instance.removePiece();
        Piece expResult = piece2;
        Piece result = instance.getPiece();
        assertEquals(expResult, result);
    }

}
