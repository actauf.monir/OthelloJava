package esi.atl.othello.monir.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class test the class Board.
 *
 * @author Monir Nasser
 */
public class BoardTest {

    public BoardTest() {
    }

    /**
     * Test of isInside method, of class Board.
     */
    @Test
    public void testIsInside() {
        System.out.println("isInside");
        Position position = new Position(5, 5);
        Board instance = new Board();
        boolean expResult = true;
        boolean result = instance.isInside(position);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsInsideWhenOutsideBoundaryUp() {
        System.out.println("testIsInsideWhenOutsideBoundaryUp");
        Position position = new Position(-1, 1);
        Board instance = new Board();
        boolean expResult = false;
        boolean result = instance.isInside(position);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsInsideWhenOutsideBoundaryDown() {
        System.out.println("testIsInsideWhenOutsideBoundaryDown");
        Position position = new Position(6, 8);
        Board instance = new Board();
        boolean expResult = false;
        boolean result = instance.isInside(position);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsInsideWhenOutsideBoundaryLeft() {
        System.out.println("testIsInsideWhenOutsideBoundaryLeft");
        Position position = new Position(1, -1);
        Board instance = new Board();
        boolean expResult = false;
        boolean result = instance.isInside(position);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsInsideWhenOutsideBoundaryRight() {
        System.out.println("testIsInsideWhenOutsideBoundaryRight");
        Position position = new Position(9, 5);
        Board instance = new Board();
        boolean expResult = false;
        boolean result = instance.isInside(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of isFree method, of class Board.
     */
    @Test
    public void testIsFreeWhenTrue() {
        System.out.println("isFree");
        Position position = new Position(5, 6);
        Board instance = new Board();
        boolean expResult = true;
        boolean result = instance.isFree(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of isFree method, of class Board.
     */
    @Test
    public void testIsFreeWhenFalse() {
        System.out.println("isFree");
        Position position = new Position(5, 6);
        Board instance = new Board();
        Piece piece = new Piece(PlayerColor.BLACK, 0);
        instance.put(piece, position);
        boolean expResult = false;
        boolean result = instance.isFree(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of isFree method, of class Board.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testIsFreeWhenPositionIsNotInside() {
        System.out.println("isFree");
        Position position = new Position(9, 6);
        Board instance = new Board();
        boolean expResult = true;
        boolean result = instance.isFree(position);
        assertEquals(expResult, result);
    }

    /**
     * Test of put method, of class Board.
     */
    @Test
    public void testPutWhenOk() {
        System.out.println("put");
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Position position = new Position(2, 2);
        Board instance = new Board();
        instance.put(piece, position);
        Piece result = instance.getPiece(position);
        assertEquals(piece, result);
    }

    /**
     * Test of put method, of class Board.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPutWhenPositionIsNotInside() {
        System.out.println("put");
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Position position = new Position(9, 2);
        Board instance = new Board();
        instance.put(piece, position);
        Piece result = instance.getPiece(position);
        assertEquals(piece, result);
    }

    /**
     * Test of put method, of class Board.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPutWhenPieceIsNull() {
        System.out.println("put");
        Piece piece = null;
        Position position = new Position(2, 2);
        Board instance = new Board();
        instance.put(piece, position);
        Piece result = instance.getPiece(position);
        assertEquals(piece, result);
    }

    /**
     * Test of put method, of class Board.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPutWhenIsNotFree() {
        System.out.println("put");
        Piece piece = new Piece(PlayerColor.BLACK, 2);
        Position position = new Position(2, 2);
        Board instance = new Board();
        instance.put(piece, position);
        instance.put(piece, position);
        Piece result = instance.getPiece(position);
        assertEquals(piece, result);
    }

    /**
     * Test of getPiece method, of class Board.
     */
    @Test
    public void testGetPiece() {
        System.out.println("getPiece");
        Position position = new Position(0, 0);
        Board instance = new Board();
        Piece expResult = new Piece(PlayerColor.BLACK, 2);
        instance.put(expResult, position);
        Piece result = instance.getPiece(position);
        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of getPieces method, of class Board.
//     */
//    @Test
//    public void testGetPieces() {
//        System.out.println("getPieces");
//        Board instance = new Board();
//        Piece[][] expResult = null;
//        Piece[][] result = instance.getPieces();
//        assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getNB_ROW method, of class Board.
     */
    @Test
    public void testGetNB_ROW() {
        System.out.println("getNB_ROW");
        Board instance = new Board();
        int expResult = 8;
        int result = instance.getNB_ROW();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNB_COLUMN method, of class Board.
     */
    @Test
    public void testGetNB_COLUMN() {
        System.out.println("getNB_COLUMN");
        Board instance = new Board();
        int expResult = 8;
        int result = instance.getNB_COLUMN();
        assertEquals(expResult, result);

    }

}
